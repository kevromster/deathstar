/**
 * Created by C3PO on 28.10.16.
 */

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 * Death Star gun - imlementation of SuperLaser.
 */
public class DeathStarGun implements SuperLaser {

    private static final double INITIAL_POWER_KOEF = 1.0;

    private double powerKoef;
    private SuperLaserMode currentMode;

    public DeathStarGun(SuperLaserMode initialMode) {
        powerKoef = INITIAL_POWER_KOEF;
        currentMode = initialMode;
    }

    public void shoot(Target target) {
        if (currentMode == SuperLaserMode.SINGLE_FIRE) {
            shootSingleFire(target);
        } else {
            throw new NotImplementedException();
        }
    }

    public SuperLaserMode getMode() {
        return currentMode;
    }

    public void setMode(SuperLaserMode mode) {
        currentMode = mode;
    }

    private void shootSingleFire(Target target) {
        // suppose implemented somehow...
    }
}
