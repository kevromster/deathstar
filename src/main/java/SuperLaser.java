/**
 * Created by R2D2 on 28.10.16.
 */

/**
 * Reprensents SuperLaser gun.
 */
public interface SuperLaser {

    /**
     * Shoots the specified target.
     * @param target the shooting target
     */
    void shoot(Target target);

    /**
     * Returns current mode of the gun.
     * @return the current mode
     */
    SuperLaserMode getMode();

    /**
     * Sets the gun mode.
     * @param mode the mode to set
     */
    void setMode(SuperLaserMode mode);
}
