/**
 * Created by R2D2 on 28.10.16.
 */

/**
 * Represents the mode of the {@link SuperLaser}.
 */
public enum SuperLaserMode {
    SINGLE_FIRE,
    VOLLEY
}
