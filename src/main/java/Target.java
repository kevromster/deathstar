/**
 * Created by R2D2 on 28.10.16.
 */

/**
 * Represents target for the {@link SuperLaser}.
 */
public interface Target {

    /**
     * Returns name of the target.
     * @return the name
     */
    String getName();

    /**
     *Returns galaxy name in which the target is located.
     * @return the galaxy name
     */
    String getGalaxy();
}
